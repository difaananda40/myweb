$(document).ready(function(){
  $(window).scroll(function() {
    if(document.body.scrollTop > 20 || document.documentElement.scrollTop > 20){
      $('.btnUp').fadeIn();
    }else{
      $('.btnUp').fadeOut();
    }
  });

  $('.btnUp').click(function() {
    $('html, body').animate({
      scrollTop: 0
    });
  });

  $('.navbar a').click(function(e) {
    if(this.hash !== ""){
      e.preventDefault();
      let hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 500, function(){
        // window.location.hash = hash;
      });
    }
  });

  var $carousel = $('.carousel').flickity({
    imagesLoaded: true,
    percentPosition: false,
  });

  var $imgs = $carousel.find('.carousel-cell img');
  // get transform property
  var docStyle = document.documentElement.style;
  var transformProp = typeof docStyle.transform == 'string' ?
    'transform' : 'WebkitTransform';
  // get Flickity instance
  var flkty = $carousel.data('flickity');

  $carousel.on( 'scroll.flickity', function() {
    flkty.slides.forEach( function( slide, i ) {
      var img = $imgs[i];
      var x = ( slide.target + flkty.x ) * -1/3;
      img.style[ transformProp ] = 'translateX(' + x  + 'px)';
    });
  });
});